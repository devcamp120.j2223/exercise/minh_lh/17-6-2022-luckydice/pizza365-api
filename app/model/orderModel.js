// bước 1: import mongoose
const mongoose = require('mongoose');

// bước 2: khai báo schema từ thư viện mongoose
const Schema = mongoose.Schema;

// bước 3: khởi tạo 1 schema với các thuộc tính được yêu cầu
const orderSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    orderCode: {
        type: String,
        unique: true,
    },
    pizzaSize: {
        type: String,
        unique: true,
    },
    pizzaType: {
        type: String,
        unique: true,
    },
    voucher: [
        {
            type: mongoose.Types.ObjectId,
            ref: "voucher"
        }
    ],
    drink: [
        {
            type: mongoose.Types.ObjectId,
            ref: "drink"
        }
    ],
    status: {
        type: String,
        unique: true,
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    }
})
module.exports = mongoose.model("orders", orderSchema)