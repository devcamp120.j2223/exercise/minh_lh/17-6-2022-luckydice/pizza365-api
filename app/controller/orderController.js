// import mongoose
const mongoose = require('mongoose');
// import modal
const orderModel = require('../model/orderModel');
const userModel = require('../model/userModel');
const drinkModel = require('../model/drinkModel');
// create new order
const createNewOrder = (request, response) => {
    //b1 thu thập dữ liệu
    let userId = request.params.userId;
    let body = request.body;
    //b2 validate
    let userEmail = request.query.email;
    //b2: validate email
    userModel.findOne({
        email: request.query.email
    }, (errorFindUser, userExist) => {
        if (errorFindUser) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: errorFindUser.message
            })
        } else {
            if (!userExist) {
                orderModel.create({
                    _id: mongoose.Types.ObjectId(),
                    orderCode: mongoose.Types.ObjectId(),
                    pizzaSize: body.pizzaSize,
                    pizzaType: body.pizzaType,
                    status: body.status,
                }, (error, data) => {
                    if (error) {
                        response.status(500).json({
                            status: "Error 500: Internal sever Error",
                            message: error.message
                        })
                    } else {
                        response.status(200).json({
                            status: "Success: create orders success",
                            data: data
                        })
                    }
                })
            }
            else {
                userModel.find({ email: request.query.email }).lean().exec((error, data) => {
                    for (let i = 0; i < data.length; i++) {
                        let newOrder = {
                            _id: data[i]._id,
                            orderCode: mongoose.Types.ObjectId(),
                            pizzaSize: body.pizzaSize,
                            pizzaType: body.pizzaType,
                            status: body.status,
                        }
                        orderModel.create(newOrder, (error, data) => {
                            if (error) {
                                response.status(500).json({
                                    status: "Error 500: Internal sever Error",
                                    message: error.message
                                })
                            } else {
                                response.status(200).json({
                                    status: "Success: create orders success",
                                    data: data
                                })
                            }
                        })
                    }

                })
            }
        }
    })
}

// get all orders
const getAllOrder = (request, response) => {

    //b3: thao tác với cơ sở dữ liệu
    orderModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        }
        else {
            response.status(200).json({
                status: "Success: Get all orders success",
                data: data
            })
        }
    })
}
// get order by id
const getOrderById = (request, response) => {
    //b1: thu thập dữ liệu
    let orderId = request.params.orderId;
    //b2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        response.status(400).json({
            status: "Error 400: bad request",
            message: "order ID is not valid"
        })
    }
    //b3: thao tác với cơ sở dữ liệu
    orderModel.findById(orderId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Get order by id success",
                data: data
            })
        }
    })
}

//update order theo id
const updateOrderById = (request, response) => {
    //b1: thu thập dữ liệu
    let orderId = request.params.orderId;
    let body = request.body;
    //b2: thu thập dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "order id is not valid"
        })
    }
    //b3: thao tác với cơ sở dữ liệu
    let orderUpdate = {
        orderCode: body.orderCode,
        pizzaSize: body.pizzaSize,
        pizzaType: body.pizzaType,
        status: body.status,
    }
    orderModel.findByIdAndUpdate(orderId, orderUpdate, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Update order success",
                data: data
            })
        }
    })
}
// xóa user dựa vào id
const deleteOrderById = (request, response) => {
    //b1: thu thập dữ liệu
    let orderId = request.params.orderId;
    //b2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "order Id is not valid"
        })
    }
    //b3: thao tác với cơ sở dữ liệu
    orderModel.findByIdAndDelete(orderId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(204).json({
                status: "Success: Delete order success"
            })
        }
    })
}
// get drink ref
const getDrinkByModel = (request, response) => {
    //b1: thu thập dữ liệu
    let bodyRequest = request.body;
    //b2: validate dữ liệu
    if (!bodyRequest.maNuocUong) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "maNuocUong is require"
        })
    }

    if (!bodyRequest.tenNuocUong) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "tenNuocUong is require"
        })
    }

    if (!(Number.isInteger(bodyRequest.donGia)) && bodyRequest.donGia > 0) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "donGia is not valid"
        })
    }

    //b3: thao tác với cơ sở dữ liệu
    let createDrink = {
        _id: mongoose.Types.ObjectId(),
        maNuocUong: bodyRequest.maNuocUong,
        tenNuocUong: bodyRequest.tenNuocUong,
        donGia: bodyRequest.donGia
    }

    drinkModel.create(createDrink, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        }
        else {
            response.status(201).json({
                status: "Success: Create drink success",
                data: data
            })
        }
    })
}

// export controller methods
module.exports = {
    createNewOrder: createNewOrder,
    getAllOrder: getAllOrder,
    getOrderById: getOrderById,
    updateOrderById: updateOrderById,
    deleteOrderById: deleteOrderById,
    getDrinkByModel: getDrinkByModel,
}